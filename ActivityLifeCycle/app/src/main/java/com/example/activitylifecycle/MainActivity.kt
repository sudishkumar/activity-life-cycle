package com.example.activitylifecycle

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val nameTextView = findViewById<TextView>(R.id.name_textView)
        val nameText = nameTextView.text
        Log.d("onCreate", "Activity is created and name is $nameText")
    }

    override fun onStart() {
        super.onStart()
        Log.d("onStart", "Activity is started")

    }

    override fun onResume() {
        super.onResume()
        Log.d("onResume", "Activity is resumed")
    }

    override fun onPause() {
        super.onPause()
        val nameTextView = findViewById<TextView>(R.id.name_textView)
        nameTextView.text ="hello bro"
        Log.d("onPause", "Activity is paused")
    }

    override fun onStop() {
        super.onStop()
        Log.d("onStop", "Activity is stop")
    }

    override fun onRestart() {
        super.onRestart()

        Log.d("onRestart", "Activity is again started")
    }

    override fun onDestroy() {
        super.onDestroy()

        Toast.makeText(this, "Hello maim where is ar", Toast.LENGTH_SHORT).show()
        Log.d("onDestroy", "Activity is destroyed")
    }
}